title: outbound-load
description: Outbound Load model definition
definitions:
  id:
    description: Unique identifier for a load. A load is one or more shipments grouped together into one or more stops that are shipped on a single piece of transport equipment.
    x-sonar-element: LoadID
    type: string
    minLength: 1
    maxLength: 10
  status:
    type: string
    enum:
      - "READY"
      - "INPROCESS"
      - "LOADING"
      - "LOADED"
      - "DISPATCHED"
      - "CANCELLED"
    default: "READY"
  metadata:
    description: Outbound Load Level Information.
    x-sonar-namespace: http://kn.swiftlog/SCShipment/0200
    type: object
    properties:
      carrier:
        description: Identifier for a carrier that delivers inbound inventory to the warehouse or outbound shipments to a customer. In the application, a carrier can be associated with a customer, order, inbound shipment, outbound shipment, load, and service levels (such as Ground, Next Day, and Saturday Delivery).
        x-sonar-element: Carrier
        type: string
        minLength: 1
        maxLength: 10
      transportMode:
        description: Name of the transport mode used to ship inventory.
        x-sonar-element: TransportMode
        type: string
        minLength: 1
        maxLength: 32
      billOfLading:
        description: Unique number that identifies the paperwork associated with shipping inventory relating to the shipment.
        x-sonar-element: DocumentNumberBOL
        type: string
        minLength: 1
        maxLength: 20
      trackingNumber:
        description: Progressive routing order (PRO) number assigned to the shipment. A PRO number is assigned as a reference to a shipment for a carrier, and is used by the carrier for most correspondence in relation to tracking the shipment.
        x-sonar-element: ProNumber
        type: string
        minLength: 1
        maxLength: 20
  appointment:
    type: object
    required:
      - id
    properties:
      id:
        description: Unique number that identifies the Appointment.
        type: string
        minLength: 1
        maxLength: 20
      start:
        description: Start Date/Time for the Appointment at the Warehouse.
        type: string
        format: date-time
      end:
        description: End Date/Time for the Appointment at the Warehouse.
        type: string
        format: date-time
  equipment:
    description: Equipment Detail information
    type: object
    required:
      - id
    properties:
      id:
        description: Alphanumeric identifier used to identify a piece of transport equipment associated with an outbound load or inbound shipment. Identifier for the carrier with which the transport equipment number is associated.
        x-sonar-element: EquipmentNumber
        type: string
        minLength: 1
        maxLength: 20
      type:
        description: Value that describes the purpose of the transport equipment, which can help is assigning an appropriate yard or dock door location.
        x-sonar-element: EquipmentDetails/Type
        type: string
        minLength: 1
        maxLength: 4
      characteristics:
        description: Transport equipment type assigned to the transport equipment. A transport equipment type identifies characteristics that are shared by individual pieces of transport equipment. For example, you can create a type for flatbed trailers, one 48 ft. rear load trailers, and another for equipment that has a liftgate.
        type: string
        minLength: 1
        maxLength: 4
      weight:
        type: object
        required:
          - value
          - uom
        properties:
          value:
            description: Current weight of the transport equipment.
            x-sonar-element: EquipmentDetails/EquipmentWeight
            type: integer
          uom:
            $ref: "./uom.v1.yaml#/definitions/weight"
      tractor:
        description: Alphanumeric identifier for a tractor. Vehicles used to haul the transport equipment, such as tractors, rail engines, and ships, are referred to as tractors. Tractor numbers are not unique, can exist multiple times, and can be associated with various carriers.
        x-sonar-element: TractorNumber
        type: string
        minLength: 1
        maxLength: 20
      driver:
        description: Name of the driver who delivered or picked up the transport equipment.
        x-sonar-element: Driver
        type: string
        minLength: 1
        maxLength: 40
      licence:
        description: Driver license number for the transport equipment driver.
        x-sonar-element: Licence
        type: string
        minLength: 1
        maxLength: 40
      seals:
        description: Identifying numbers that are displayed on the seals or tags used to close shipping transport equipment.
        x-sonar-element:
          - Seal1
          - Seal2
          - Seal3
          - Seal4
        type: array
        minItems: 1
        maxItems: 4
        items:
          type: string
          minLength: 1
          maxLength: 30
  stop:
    id:
      description: Unique identifier for a stop. A stop is a collection of one or more outbound shipments making up a delivery to a single customer.
      x-sonar-element: StopID
      type: string
      minLength: 1
      maxLength: 10
    metadata:
      description: Outbound Stop Level Information.
      type: object
      required:
        - sequence
      properties:
        sequence:
          description: A unique identifier that determines the order in which the stops must be loaded into the transport equipment.
          x-sonar-element: StopSequence
          type: integer
        documentNumber:
          description: Unique number that identifies the paperwork associated with shipping inventory relating to the shipment.
          x-sonar-element: DocumentNumber
          type: string
          minLength: 1
          maxLength: 20
        seal:
          description: Identification number of the stop seal assigned to the stop when the application immediately loads the stop.
          x-sonar-element: StopSeal
          type: string
          minLength: 1
          maxLength: 30
  events:
    type:
      type: object

type: object
required:
  - warehouse
  - client
  - id
  - status
  - metadata
properties:
  warehouse:
    $ref: "./warehouse.v1.yaml#/definitions/id"
  client:
    $ref: "./client.v1.yaml#/definitions/id"
  id:
    $ref: "#/definitions/id"
  callback:
    $ref: "./object.v1.yaml#/definitions/callback"
  status:
    $ref: "#/definitions/status"
  metadata:
    $ref: "#/definitions/metadata"
  appointment:
    $ref: "#/definitions/appointment"
  equipment:
    $ref: "#/definitions/equipment"
  references:
    $ref: "./reference.v1.yaml#/definitions/map"
  notes:
    $ref: "./note.v1.yaml#/definitions/map"
  stops:
    type: array
    minItems: 1
    items:
      type: object
      required:
        - id
        - metadata
        - shipments
      properties:
        id:
          $ref: "#/definitions/stop/id"
        metadata:
          $ref: "#/definitions/stop/metadata"
        shipments:
          type: array
          minItems: 1
          items:
            type: object
            required:
              - shipment
              - handlingUnits
            properties:
              shipment:
                $ref: "./outbound-shipment.v1.yaml#/definitions/id"
              handlingUnits:
                type: array
                minItems: 1
                items:
                  $ref: "./handling-unit.v1.yaml#/definitions/object"

  events:
    type: array
    minItems: 1
    items:
      $ref: "#/definitions/events/type"
