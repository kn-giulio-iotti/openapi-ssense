title: inbound-shipment
description: Inbound Shipment model definition
definitions:
  id:
    description: Unique identifier used for inventory tracking for an inbound shipment of inventory. An inbound shipment is a group of orders that are transported to the warehouse together or received together. When inventory arrives at the warehouse on a piece of transport equipment, an inbound shipment represents the contents of the transport equipment; however, one or more inbound shipments can be associated with a piece of transport equipment.
    x-sonar-element: InboundShipment
    type: string
    minLength: 1
    maxLength: 20
  status:
    type: string
    enum:
      - "READY"
      - "RECEIVING"
      - "CLOSED"
      - "CANCELLED"
    default: READY
  metadata:
    description: Inbound Shipment Level Information.
    x-sonar-namespace: http://kn.swiftlog/InboundShipment/0200
    type: object
    required:
      - carrier
    properties:
      reference:
        description: Additional information that identifies a group of inbound shipments. For example, a carrier is a typical inbound shipment reference.
        x-sonar-element: InboundShipmentReference
        type: string
        minLength: 1
        maxLength: 20
      carrier:
        description: Identifier for a carrier associated with this Inbound Shipment.
        x-sonar-element: Carrier
        type: string
        minLength: 1
        maxLength: 10
      masterBillOfLading:
        description: Master BOL
        type: string
        minLength: 1
        maxLength: 20
      billOfLading:
        description: Unique number that identifies the paperwork associated with shipping inventory relating to the shipment.
        x-sonar-element: DocumentNumberBOL
        type: string
        minLength: 1
        maxLength: 20
      trackingNumber:
        description: Progressive routing order (PRO) number assigned to the shipment. A PRO number is assigned as a reference to a shipment for a carrier, and is used by the carrier for most correspondence in relation to tracking the shipment.
        x-sonar-element: ProNumber
        type: string
        minLength: 1
        maxLength: 20
      grossWeight:
        type: object
        required:
          - value
          - uom
        properties:
          value:
            description: Weight of the inventory on the shipment that includes its packaging and the containers on which it is shipped.
            x-sonar-element: GrossWeight
            type: integer
          uom:
            $ref: "./uom.v1.yaml#/definitions/weight"
      pallets:
        description: Number of pallets on the inbound shipment.
        x-sonar-element: NumberOfPallets
        type: integer
      cases:
        description: Number of cases of inventory on the inbound shipment
        x-sonar-element: NumberOfCases
        type: integer
      freight:
        description: Freight Cost
        x-sonar-element: FreightCost
        $ref: "./cost.v1.yaml#/definitions/type"
      expectedDate:
        description: Date on which the inbound shipment or inbound order is expected to arrive at the warehouse.
        x-sonar-element: ExpectedDate
        type: string
        format: date-time
      shippedDate:
        description: Date on which the inbound shipment originated.
        x-sonar-element: ShippedDate
        type: string
        format: date-time
  equipment:
    description: Equipment Detail information
    type: object
    required:
      - id
    properties:
      id:
        description: Alphanumeric identifier used to identify a piece of transport equipment associated with an outbound load or inbound shipment. Identifier for the carrier with which the transport equipment number is associated.
        x-sonar-element: EquipmentNumber
        type: string
        minLength: 1
        maxLength: 20
      type:
        description: Value that describes the purpose of the transport equipment, which can help is assigning an appropriate yard or dock door location.
        x-sonar-element: EquipmentDetails/Type
        type: string
        minLength: 1
        maxLength: 4
      characteristics:
        description: Transport equipment type assigned to the transport equipment. A transport equipment type identifies characteristics that are shared by individual pieces of transport equipment. For example, you can create a type for flatbed trailers, one 48 ft. rear load trailers, and another for equipment that has a liftgate.
        type: string
        minLength: 1
        maxLength: 4
      weight:
        type: object
        required:
          - value
          - uom
        properties:
          value:
            description: Current weight of the transport equipment.
            x-sonar-element: EquipmentDetails/EquipmentWeight
            type: integer
          uom:
            $ref: "./uom.v1.yaml#/definitions/weight"
      tractor:
        description: Alphanumeric identifier for a tractor. Vehicles used to haul the transport equipment, such as tractors, rail engines, and ships, are referred to as tractors. Tractor numbers are not unique, can exist multiple times, and can be associated with various carriers.
        x-sonar-element: TractorNumber
        type: string
        minLength: 1
        maxLength: 20
      driver:
        description: Name of the driver who delivered or picked up the transport equipment.
        x-sonar-element: Driver
        type: string
        minLength: 1
        maxLength: 40
      licence:
        description: Driver license number for the transport equipment driver.
        x-sonar-element: Licence
        type: string
        minLength: 1
        maxLength: 40
      seals:
        description: Identifying numbers that are displayed on the seals or tags used to close shipping transport equipment.
        x-sonar-element:
          - Seal1
          - Seal2
          - Seal3
          - Seal4
        type: array
        minItems: 1
        maxItems: 4
        items:
          type: string
          minLength: 1
          maxLength: 30
  appointment:
    description: Appointment
    type: object
    minProperties: 1
    properties:
      id:
        description: Unique number that identifies the Appointment.
        x-sonar-element: AppointmentID
        type: string
        minLength: 1
        maxLength: 20
      start:
        description: Start Date/Time for the Appointment at the Warehouse.
        x-sonar-element: AppointmentStartDate
        type: string
        format: date-time
      end:
        description: End Date/Time for the Appointment at the Warehouse.
        x-sonar-element: AppointmentEndDate
        type: string
        format: date-time


  events:
    inventoryReceiptEvent:
      x-implemented: true
      allOf:
        - $ref: "./event.v1.yaml#/definitions/type"
        - type: object
          required:
            - type
            - payload
          properties:
            type:
              type: string
              default: "InboundShipmentInventoryReceipt"
            payload:
              type: object
              required:
                - receipts
              properties:
                receipts:
                  type: array
                  minItems: 1
                  items:
                    type: object
                    required:
                      - id
                      - items
                    properties:
                      id:
                        $ref: "./inbound-receipt.v1.yaml#/definitions/id"
                      items:
                        type: array
                        minItems: 1
                        items:
                          type: object
                          required:
                            - item
                            - quantities
                          properties:
                            item:
                              $ref: "./item.v1.yaml#/definitions/id"
                            quantities:
                              type: array
                              minItems: 1
                              items:
                                allOf:
                                  - $ref: "./inventory-details.v1.yaml#/definitions/quantity"
                                  - $ref: "./inventory-details.v1.yaml#/definitions/state"
    inboundShipmentClosedEvent:
      x-implemented: false
      allOf:
        - $ref: "./event.v1.yaml#/definitions/type"
        - type: object
          required:
            - type
            - payload
          properties:
            type:
              type: string
              default: "InboundShipmentClosed"
            payload:
              type: object
              required:
                - receipts
              properties:
                receipts:
                  type: array
                  minItems: 1
                  items:
                    type: object
                    required:
                      - id
                      - items
                    properties:
                      id:
                        $ref: "./inbound-receipt.v1.yaml#/definitions/id"
                      items:
                        type: array
                        minItems: 1
                        items:
                          type: object
                          required:
                            - item
                            - quantities
                          properties:
                            item:
                              $ref: "./item.v1.yaml#/definitions/id"
                            quantities:
                              type: array
                              minItems: 1
                              items:
                                allOf:
                                  - $ref: "./inventory-details.v1.yaml#/definitions/quantity"
                                  - $ref: "./inventory-details.v1.yaml#/definitions/state"
    type:
      anyOf:
        - $ref: "#/definitions/events/inventoryReceiptEvent"
        - $ref: "#/definitions/events/inboundShipmentClosedEvent"
type: object
required:
  - warehouse
  - client
  - id
  - status
  - metadata
  - receipts
properties:
  warehouse:
    $ref: "./warehouse.v1.yaml#/definitions/id"
  client:
    $ref: "./client.v1.yaml#/definitions/id"
  id:
    $ref: "#/definitions/id"
  callback:
    $ref: "./object.v1.yaml#/definitions/callback"
  status:
    $ref: "#/definitions/status"
  carrier:
    $ref: "./address.v1.yaml#/definitions/type"
  metadata:
    $ref: "#/definitions/metadata"
  equipment:
    $ref: "#/definitions/equipment"
  appointment:
    $ref: "#/definitions/appointment"
  references:
    $ref: "./reference.v1.yaml#/definitions/map"
  notes:
    $ref: "./note.v1.yaml#/definitions/map"
  receipts:
    type: array
    minItems: 1
    items:
      type: object
      required:
        - receipt
        - items
      properties:
        receipt:
          type: object
          required:
            - id
            - supplier
            - metadata
          properties:
            id:
              $ref: "./inbound-receipt.v1.yaml#/definitions/id"
            supplier:
              $ref: "./inbound-receipt.v1.yaml#/definitions/supplier"
            metadata:
              $ref: "./inbound-receipt.v1.yaml#/definitions/metadata"
            references:
              $ref: "./reference.v1.yaml#/definitions/map"
            notes:
              $ref: "./note.v1.yaml#/definitions/map"
        items:
          type: array
          minItems: 1
          items:
            type: object
            required:
              - item
              - expected
            properties:
              item:
                $ref: "./item.v1.yaml#/definitions/id"
              expected:
                oneOf:
                  - allOf:
                    - $ref: "./inventory-details.v1.yaml#/definitions/quantity"
                    - $ref: "./inventory-details.v1.yaml#/definitions/state"
                  - type: array
                    minItems: 1
                    items:
                      allOf:
                        - $ref: "./inventory-details.v1.yaml#/definitions/quantity"
                        - $ref: "./inventory-details.v1.yaml#/definitions/state"
              references:
                $ref: "./reference.v1.yaml#/definitions/map"
              notes:
                $ref: "./note.v1.yaml#/definitions/map"
              received:
                oneOf:
                  - allOf:
                    - $ref: "./inventory-details.v1.yaml#/definitions/quantity"
                    - $ref: "./inventory-details.v1.yaml#/definitions/state"
                  - type: array
                    minItems: 1
                    items:
                      allOf:
                        - $ref: "./inventory-details.v1.yaml#/definitions/quantity"
                        - $ref: "./inventory-details.v1.yaml#/definitions/state"
        references:
          $ref: "./reference.v1.yaml#/definitions/map"
        notes:
          $ref: "./note.v1.yaml#/definitions/map"
  events:
    type: array
    minItems: 1
    items:
      $ref: "#/definitions/events/type"
